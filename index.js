const Telegraf = require('telegraf')
const fs = require('fs')
const conf = require('./config')

const stateFilePath = __dirname + '/state.json'
const userEmojis = ['🤖', '👽', '💩', '😺', '💀', '👑', '🐶', '🐸', '🐤', '🍕', '⚾️', '🦋', '🐖', '🐬', '🍔']

const FOUNDER_BALANCE = 1000000000
const AUTOSAVE_TIMEOUT = (conf.autosaveFrequency || 300) * 1000
const INITIAL_BALANCE = conf.initialBalance === undefined ? 10 : conf.initialBalance
const AIRDROP_FREQUENCY = (conf.airdropFrequency || 3600) * 1000
const AIRDROP_COOLDOWN = (conf.airdropCooldown === undefined ? 10 : conf.airdropCooldown) * 1000
const AIRDROP_RANGE = conf.airdropRange === undefined ? 10 : conf.airdropRange
const AIRDROP_REWARD = conf.airdropReward === undefined ? 5 : conf.airdropReward
const AIRDROP_FEE = conf.airdropFee === undefined ? 0 : conf.airdropFee
const AIRDROP_FEE_INCREASE = conf.airdropFeeIncrease === undefined ? 1 : conf.airdropFeeIncrease

const txType = {
  TRANSACTION: 0,
  INITIAL: 1,
  AIRDROP_REWARD: 2,
  AIRDROP_FEE: 3,
  BURN: 4
}

const calls = {
  SEND: /^pp @?(\w+)(?: ([0-9]*))?$/ig,
  WALLET: /^pp wallet(?: @?(\w+))?/i,
  BURN: /pp burn(?: ([0-9]*))?$/i,
  STATS: /^pp stats$/ig,
  TRANSACTIONS: /^pp transactions$/i,
  WALLETS: /^pp wallets$/ig,
  AIRDROP: /^pp airdrop( [0-9]+)?$/i
}

const transactions = []
const wallets = []
const idToWallet = {}
const idToUsername = {}
const idToFirstName = {}
const userIds = []
const usernameToId = {}

let lastAirdrop
let correctAirdrop
const idToAirdropCooldown = {}
const idToAirdropAttempts = {}

class BotError {
  constructor(code) {
    this.message = code
  }
}

const saveState = () => {
  console.log('Saving state...')
  const json = {
    wallets,
    transactions,
    users: userIds.map(id => ({
      id,
      username: idToUsername[id],
      first_name: idToFirstName[id]
    }))
  }
  fs.writeFileSync(stateFilePath, JSON.stringify(json))
  console.log('State saved')
}

const loadState = () => {
  if (!fs.existsSync(stateFilePath)) {
    return
  }
  console.log('Loading state...')

  const source = fs.readFileSync(stateFilePath).toString()
  const state = JSON.parse(source)

  while (transactions.length) {transactions.pop()}
  while (wallets.length) {wallets.pop()}
  while (userIds.length) {userIds.pop()}

  for (let i = 0; i < state.transactions.length; i++) {
    transactions.push(state.transactions[i])
  }
  for (let i = 0; i < state.wallets.length; i++) {
    const wallet = state.wallets[i]
    wallets.push(wallet)
    idToWallet[wallet.userId] = wallet
  }
  for (let i = 0; i < state.users.length; i++) {
    handleUser(state.users[i])
  }

  const lastAirdropDate = transactions
    .filter(tx => tx.type === txType.AIRDROP_REWARD)
    .map(tx => tx.date)
    .sort((a, b) => a.date - b.date)
    .pop()

  if (lastAirdropDate) {
    lastAirdrop = lastAirdropDate
  }

  console.log('State loaded')
}

const handleError = (ctx, err) => {
  if (err instanceof BotError) {
    console.log('Bot error', err.message)  
    return ctx.replyWithMarkdown(parseMessage(err.message))
  }
  console.error(err)
}

const parseMessage = (msg) => msg.replace(/_/ig, '\\_')

const getReadableName = (userId) => {
  const username = idToUsername[userId]
  const first_name = idToFirstName[userId]
  const displayName = username ? username : first_name
  const emoji = userEmojis[userIds.indexOf(userId) % userEmojis.length]

  // cool except for ugly thumbnail:
  //return `[${first_name}](t.me/${username})`

  return `${username ? `@${username}` : first_name} ${emoji}`
}

const createWallet = (userId) => {
  const balance = wallets.length === 0 ? FOUNDER_BALANCE : INITIAL_BALANCE
  const wallet = {
    userId, 
    balance
  }
  wallets.push(wallet)
  idToWallet[userId] = wallet
  return wallet
}

const resetAirdrop = () => {
  lastAirdrop = Date.now()
  correctAirdrop = 1 + Math.random() * (AIRDROP_RANGE-1) | 0

  for (let i = userIds.length; i--;) {
    idToAirdropAttempts[userIds[i]] = 0
  }
}

const transfer = (sender, receiver, value, type) => {
  const senderWallet = idToWallet[sender]
  const receiverWallet = idToWallet[receiver]
  if (!senderWallet) {
    throw new BotError(`Wallet ID ${sender} not found`)
  }
  if (!receiverWallet) {
    throw new BotError(`Wallet ID ${receiver} not found`)
  }
  if (sender === receiver) {
    throw new BotError(`You can't send PP to yourself`)
  }
  if (senderWallet.balance < value) {
    throw new BotError(`Insufficient balance`)
  }
  if (type === txType.TRANSACTION && value <= 0) {
    throw new BotError(`Minimum PP tx is 1`)
  }
  const tx = {
    date: Date.now(),
    sender: senderWallet.userId,
    receiver: receiverWallet.userId,
    value,
    type
  }
  senderWallet.balance -= value
  receiverWallet.balance += value
  transactions.push(tx)
  return tx
}

const getMentions = (ctx, next) => {
  try {
    const {entities} = ctx.update.message
    ctx.mentions = !entities ? [] : entities.map((e, i) => {
      const mention = ctx.match[1 + i]
      const id = !e.user ? usernameToId[mention] : e.user.id
      if (!id) {
        throw new BotError(`Couldn't find a Palpoint account belonging to @${mention}`)
      }
      return id
    })
    return next()
  } catch (err) {
    return handleError(ctx, err)
  }
}

const handleUser = (user, y) => {
  const {id, username, first_name} = user

  if (userIds.indexOf(id) === -1) {
    userIds.push(id)
  }

  if (username) {
    usernameToId[username] = id
    idToUsername[id] = username
  }

  idToFirstName[id] = first_name

  if (typeof idToAirdropAttempts[id] === 'undefined') {
    idToAirdropAttempts[id] = 0
  }

  if (!idToWallet[id]) {
    createWallet(id)
  }
}

const onlyAdmin = (ctx, next) => {
  if (ctx.update.message.from.username === (conf.adminUsername || 'harsox')) {
    return next()
  }
}

const timeDifference = (dateFrom, dateTo) => {
  const ms = dateTo - dateFrom
  
  const [s, m, h] = [
    Math.ceil(ms / 1000),
    Math.floor(ms / 1000 / 60),
    Math.floor(ms / 1000 / 60 / 60)
  ].map(Math.abs)

  const timeText = 
    h >= 1  ? `${h}h`
  : s >= 60 ? `${m}m`
  : `${s}s`
  
  return ms<0 ? `${timeText} ago` : `in ${timeText}`
}

const bot = new Telegraf(conf.token)

bot.telegram.getMe().then(botInfo => {
  resetAirdrop()
  loadState()
  setInterval(saveState, AUTOSAVE_TIMEOUT)
  handleUser(botInfo)
  bot.options.userId = botInfo.id
}).catch(err => {
  console.error(err)
})

bot.use(async (ctx, next) => {
  handleUser(ctx.update.message.from)
  return next()
})

bot.hears('pp save', onlyAdmin, ctx => {
  saveState()
})

bot.hears('pp load', onlyAdmin, ctx => {
  loadState()
})

bot.hears(calls.SEND, getMentions, (ctx, next) => {
  try {
    const chatId = ctx.update.message.chat.id
    const senderId = ctx.update.message.from.id
    const receivers = ctx.mentions
    const value = ctx.match[2] ? parseInt(ctx.match[2]) : 1

    // no users
    if (!receivers.length) {
      return next()
    }

    // send
    receivers.forEach(id => transfer(senderId, id, value, txType.TRANSACTION))
    const receiversText = receivers.map(getReadableName).join(', ')
    const message = `${getReadableName(senderId)} → ${receiversText} *${value}PP*`
    return ctx.replyWithMarkdown(parseMessage(message))
  } catch (err) {
    return handleError(ctx, err)
  }
})

bot.hears(calls.BURN, (ctx, next) => {
  try {
    const userId = ctx.update.message.from.id
    const value = ctx.match[1] ? parseInt(ctx.match[1]) : 1
    transfer(userId, bot.options.userId, value, txType.BURN)
    const message = `${getReadableName(userId)} burned *${value} PP* 😂`
    ctx.replyWithMarkdown(parseMessage(message))
  } catch (err) {
    return handleError(ctx, err)
  }
})

bot.hears(calls.STATS, ctx => {
  try {
    const numTransactions = transactions.filter(t => t.type === txType.TRANSACTION).length
    const numAirdropsRewards = transactions.filter(t => t.type === txType.AIRDROP_REWARD).length
    const numAirdropsAttempts = transactions.filter(t => t.type === txType.AIRDROP_FEE).length
    let message = '*Palpoint statistics*\n'
    message += `Wallets: *${wallets.length}*\n`
    message += `Transactions: *${numTransactions}*\n`
    message += `Airdrop rewards: *${numAirdropsRewards}*\n`
    message += `Airdrop attempts: *${numAirdropsAttempts}*\n`
    message += `\n`
    ctx.replyWithMarkdown(parseMessage(message))
  } catch (err) {
    return handleError(ctx, err)
  }
})

bot.hears(calls.TRANSACTIONS, ctx => {
  try {
    let message = `*Palpoint recent transactions*\n`
    message += transactions
      .filter(tx => tx.type === txType.TRANSACTION)
      .sort((a, b) => (b.date - a.date))
      .slice(0, 5)
      .map(tx => 
        `${getReadableName(tx.sender)} → ${getReadableName(tx.receiver)} *${tx.value}PP* ${timeDifference(Date.now(), tx.date)}`
        )
      .join('\n')
    return ctx.replyWithMarkdown(parseMessage(message))
  } catch (err) {
    return handleError(ctx, err)
  }
})

bot.hears(calls.WALLETS, ctx => {
  try {
    let message = `*Palpoint wallets*\n`
    message += wallets.map(wallet => {
      const receivedAmount = transactions
        .filter(t => t.type === txType.TRANSACTION && t.receiver === wallet.userId)
        .reduce((p, t) => p + t.value, 0)
      return `${getReadableName(wallet.userId)} (*${wallet.balance}PP*, *+${receivedAmount}PP*)`
    }).join('\n')
    return ctx.replyWithMarkdown(parseMessage(message))
  } catch (err) {
    return handleError(ctx, err)
  }
})

bot.hears(calls.WALLET, getMentions, ctx => {
  try {
    const userId = ctx.mentions[0] ? ctx.mentions[0] : ctx.update.message.from.id
    const wallet = idToWallet[userId]
    const sentTx = transactions.filter(t => t.type === txType.TRANSACTION && t.sender === userId)
    const receivedTx = transactions.filter(t => t.type === txType.TRANSACTION && t.receiver === userId)
    const sentAmount = sentTx.reduce((p, t) => p + t.value, 0)
    const receivedAmount = receivedTx.reduce((p, t) => p + t.value, 0)
    // compose message
    let message = `*${getReadableName(userId)} wallet*\n`
    message += `Current Balance: *${wallet.balance} PP*\n`
    message += `Total Received: *${receivedAmount} PP*\n`
    message += `Total Sent: *${sentAmount} PP*\n`
    return ctx.replyWithMarkdown(parseMessage(message))
  } catch (err) {
    return handleError(ctx, err)
  }
})

bot.hears(calls.AIRDROP, ctx => {
  try {
    const nextAirdrop = lastAirdrop + AIRDROP_FREQUENCY

    if (nextAirdrop > Date.now()) {
      throw new BotError(`New airdrop ${timeDifference(Date.now(), nextAirdrop)}`)
    }
    
    if (!ctx.match[1]) {
      let message = `*Airdrop Info*\n`
      message += `Reward: *${AIRDROP_REWARD} PP*\n`
      message += `Starting fee: *${AIRDROP_FEE} PP*\n`
      message += `Fee increase: *${AIRDROP_FEE_INCREASE} PP*\n`
      message += `Guess range: *1 - ${AIRDROP_RANGE}*\n\n`
      message += `_"pp airdrop <guess>"_\n`
      return ctx.replyWithMarkdown(message)
    }

    const userId = ctx.update.message.from.id
    const guess = parseInt(ctx.match[1])
    const correct = correctAirdrop === guess
    const numAttempts = idToAirdropAttempts[userId]
    const currentFee = AIRDROP_FEE + AIRDROP_FEE_INCREASE * numAttempts
    const nextFee = currentFee + AIRDROP_FEE_INCREASE
    const totalCost = (Math.pow(currentFee, 2) + currentFee) / 2

    if (guess < 1 || guess > AIRDROP_RANGE) {
      throw new BotError(`Guess range: *1 - ${AIRDROP_RANGE}*`)
    }

    if (idToAirdropCooldown[userId]) {
      if (Date.now() < idToAirdropCooldown[userId] + AIRDROP_COOLDOWN) {
        throw new BotError(`You guessed too recently. Try again ${timeDifference(Date.now(), idToAirdropCooldown[userId] + AIRDROP_COOLDOWN)}`)
      }
    }
    
    transfer(userId, bot.options.userId, currentFee, txType.AIRDROP_FEE)
    idToAirdropCooldown[userId] = Date.now()
    idToAirdropAttempts[userId]++

    let message = `Guess: *${guess}* ${correct ? '🎉' : '👎'}\n`
    message += `Total cost: *${!totalCost ? 'FREE' : totalCost + 'PP'}*\n`
    if (correct) {
      message += `Reward: *${AIRDROP_REWARD}PP*\n\n`
      message += `Another airdrop ${timeDifference(Date.now(), lastAirdrop + AIRDROP_FREQUENCY)}`
    } else {
      message += `Next fee: *${!nextFee ? 'FREE' : nextFee + 'PP'}*\n`
      message += `Attempts: ${numAttempts + 1}\n\n`
      message += `Guess again ${timeDifference(Date.now(), idToAirdropCooldown[userId] + AIRDROP_COOLDOWN)}`
    }

    if (correct) {
      transfer(bot.options.userId, userId, AIRDROP_REWARD, txType.AIRDROP_REWARD)
      resetAirdrop()
    }

    return ctx.replyWithMarkdown(parseMessage(message))
  } catch (err) {
    return handleError(ctx, err)
  }
})

bot.catch(err => {
  return console.error('err', err)
})

bot.startPolling()
